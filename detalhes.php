<?php
  include 'header.php';
  include 'model/Produto.php';
     $id = $_REQUEST['id'];
     $produto = new Produto();
     $resultado = $produto->fetch("SELECT * FROM produto WHERE id = $id");

    // $total = $produto->fetch("SELECT COUNT(id) AS id FROM produto WHE");
    // $total = $total->fetch_all(MYSQLI_ASSOC);
    // $total = $total[0]['id'];
     while ($row = $resultado->fetch_array()){
      $nome = $row['nome'];
      $imagem = $row['imagem'];
      $preco = $row['preco'];
      $qtd = $row['quantidade'];
      $qtd2 = $qtd; //variável para disponibilizar a mensagem Produto Esgotado
      $descricao = $row['descricao'];
     }
?>



<!DOCTYPE html>
<html>
 <title>Detalhes</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.css" type='text/css'>
  <link rel="stylesheet" href="font-awesome/fonts/FontAwesome.otf">


<body>
  
  
     <div class="container">

          
            <h5 class="my-5"><?php echo $nome;?></h5>


            <?php if($qtd2 = 0): ?>
             <div class="alert alert-danger" role="alert">
                <i class="fa fa-info-circle"></i>
                Produto esgotado
             </div>
            <?php endif;?>
          <hr>

          <div class="row">


              <div class="col-12 col-md-9 mb-3">
                <div class="text-center">
                   <img src=<?php echo "admin/produtos/".$imagem;?> alt="iphone11" class="img-thumbnail">
                </div>
               
              </div>

              <div class="col-12 col-md-3">
                <div class="row ml-1">
                  <h6>Preço: </h6>
                    <h6 class="ml-2 mb-5 text-danger"><?php echo number_format($preco,2,',','.');?> MT</h6>
                </div>
                 

                 <form action="">
               
                <div class="form-group">
                  <label for="">Quantidade:</label>
                  <select class="form-control mb-5" name="" id="">
                    <!-- POPULANDO O COMBOBOX -->
                    <?php 
                      for ($i = 1; $i<= $qtd; $i++):
                    ?>

                    <option value="preto"><?php echo $i;?></option>

                    <?php 
                       endfor;
                    ?>
                    <!-- FIM DO FOR -->
                  </select>
                </div>

                <div class="text-center">
                  <button type="button" class="btn btn-primary mb-3"><img src="img/carrinha.svg">Comprar</button>
                </div>
                
                   
             
            </form> 
              </div>

          </div>
          
        
            <hr>
              <h5 class="mb-5">Descrição:</h5>
              <pre class="ml-5 mb-5"><?php echo $descricao;?></pre>
            
            
     </div> 

        <!-- FOOTER -->
            <?php 
              include 'footer.php';
            ?>
             



    <!-- JAVASCRIPT & JQUERY -->
  <script src="js/jquery/jquery.min.js"></script>
  <script src="js/popperjs/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
      
</body>
</html>

