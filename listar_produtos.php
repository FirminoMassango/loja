<!DOCTYPE html>
<html>

<body>
  
    <!-- LADO DIREITO -->
        <div class="row col-12 col-sm-4 ml-2">
          <div class="mb-3">
            <h5>NOVO</h5>
            <div class="mb-3" id="underline-new"></div>
          </div>
        </div>
        
     
      <div class="row justify-content-sm-center">

                  <!-- PHP -->
          <?php while ($row = $resultado_novo->fetch_array()):?>
              <div class="produto mb-4 ml-2 mr-2">
                <img class="imgProduto mb-2" src=<?php echo "admin/produtos/".$row['imagem'];?> alt="produto" width="200px" height="200px">
                  <p class="modelo mb-1"><?php echo $row['nome'];?></p>
                  <p class="preco"><?php echo number_format($row['preco'],2,',','.');?> MT</p>
                  <!-- <a id="detalhes" href="#" data-toggle="modal" data-target="#siteModal">DETALHES</a> -->
                   <a id="detalhes" href="detalhes.php?id=<?php echo $row['id'];?>">DETALHES</a>
              </div>

          <?php endwhile;?>   <!-- FIM PHP -->
       
        
      </div>

      <!-- PAGINAÇÃO -->
      <div class="row justify-content-center mt-5">
        <div class="paginacaoManual">
          <div class="row">
         
            <a href="#" title="Anterior"><i class="fa fa-arrow-left mr-1 text-secondary"></i></a>
            <span class="ml-3 mr-3">1 / <?php echo ceil($total/$limite)?></span>
           
            <a href="index.php?<?=$i;?>" title="Próximo"><i class="fa fa-arrow-right ml-1 text-secondary"></i></a>
        

          </div>
        </div>
      </div>

      <!-- OUTROS -->
      <div class="container">
        <div class="row col-12 col-sm-4 ml-2">
          <div class="mb-5 mt-5">
            <h5>OUTROS</h5>
            <div class="bg-success" id="underline-other"></div>
          </div>
        </div>
        </div>

        <div class="row justify-content-sm-center">
      
            <!-- PHP -->
    <?php while ($row = $resultado_outros->fetch_array()):?>
        <div class="produto mb-4 ml-2 mr-2">
          <img class="imgProduto mb-2" src=<?php echo "admin/produtos/".$row['imagem'];?> alt="produto" width="200px" height="200px">
            <p class="modelo mb-1"><?php echo $row['nome'];?></p>
            <p class="preco"><?php echo number_format($row['preco'],2,',','.');?> MT</p>
           <!--  <a href="#index?id=<?php echo $id?>" id="detalhes" data-toggle="modal" data-target="#siteModal" <?php $id=$row['id'];?>>DETALHES</a> -->
                <a id="detalhes" href="detalhes.php?id=<?php echo $row['id'];?>">DETALHES</a>
        </div>

            <?php 

              
              $nome = $row['nome'];
              $preco = $row['preco'];
              $descricao = $row['descricao'];
              $qtd = $row['quantidade'];
              $imagem ="admin/produtos/".$row['imagem'];


            ?>
             <!-- FIM PHP --> <?php endwhile;?>   <!-- FIM PHP -->

 
        <!-- PAGINAÇÃO -->
        <div class="container">
          <div class="row justify-content-center mb-5">
            <div class="paginacaoManual">
              <div class="row">
                <a href="#" title="Anterior"><i class="fa fa-arrow-left mr-1 text-secondary"></i></a>
                <span class="ml-3 mr-3">1 / <?php echo ceil($total/$limite_todos)?></span>
                <a href="#" title="Próximo"><i class="fa fa-arrow-right ml-1 text-secondary"></i></a>
              </div>
            </div>
          </div>
        </div>
  

    </div>



 </div>
  
 <!-- MODAL -->
 <div class="modal fade" id="siteModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title"><?php echo $id;?></h5>

        <h5 class="modal-title"><?php echo $nome;?></h5>
        <button type="button" class="close" data-dismiss="modal">
          <span>&times;</span>
        </button>
      </div>

      <div class="modal-body">
       <div class="alert alert-danger" role="alert">
          <i class="fa fa-info-circle"></i>
          Produto esgotado
       </div>
       
       <div class="container">
        <div class="row">
       
          <div class="col-12 col-md-6 mb-3">
            <img src=<?php echo $imagem?> alt="iphone11" class="img-thumbnail">
          </div>

          <div class="col-12 col-md-6">
            <p><strong>Preço: </strong><?php echo number_format($preco,2,',','.');?> MT</p>
            <hr>
            <p><strong>Descrição:</strong></p>
            <pre>
              <?php echo $descricao;?>
            </pre>
            <hr>
             <form action="">
               <div class="form-group">
                  <label for="">Cor:</label>
                  <select class="form-control" name="" id="">
                    <option value="preto">Preto</option>
                    <option value="branco">Branco</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Quantidade:</label>
                  <select class="form-control" name="" id="">
                    <!-- POPULANDO O SELECT -->
                    <?php 

                      for ($i = 1; $i<= $qtd; $i++):
                    ?>

                    <option value="preto"><?php echo $i;?></option>

                    <?php 
                       endfor;
                    ?>
                    <!-- FIM DO FOR -->
                  </select>
                </div>

             
            </form> 

           
          </div>
          
        </div>  
       </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Temporibus</button>
        <button type="button" class="btn btn-primary"><img src="img/carrinha.svg" style="width: 28px;">Comprar</button>
      </div>
    </div>
  </div>
</div>



    <!-- JAVASCRIPT & JQUERY -->
  <script src="js/jquery/jquery.min.js"></script>
  <script src="js/popperjs/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
      
</body>
</html>

