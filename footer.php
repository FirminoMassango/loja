<!DOCTYPE html>
<html>

<body>

  <!-- RODAPÉ -->
 
  <footer>
    <hr>
    <div class="container-fluid text-dark" style="background-color: #EEEEEE">
      <div class="row col-12 pt-5">
        <div class="col-sm-4">
          <h3>Info</h3>
          <p>
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
            fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in 
            culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <p>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
            laudantium.</p>
        </div>
  
        <div class="col-sm-4 mb-3" id="menu">
          <h3>Menu</h3>
          
          <div class="list-group text-center">
            <a class="list-group-item list-group-item-action" href="contacto.php">Contacto</a>
            <a class="list-group-item list-group-item-action" href="#">Sobre Nós</a>
            <a class="list-group-item list-group-item-action" href="#">Outro</a>
          </div>
        </div>
  
        <div class="col-sm-3 ml-5">
          <h3>Siga-nos:</h3>
          <div class="row text-center">
            <p><a class="text-light" href="#" title="Facebook"><i class="text-danger mt-5 fa fa-facebook-square fa-lg"></i></a></p>
            <p><a class="text-light" href="#" title="Instagram"><i class="text-danger ml-5 mt-5 fa fa-instagram fa-lg"></i></a></p>
            <p class="ml-2"><a class="text-light" href="#" title="Twitter"><i class="text-danger ml-5 mt-5 fa fa-twitter fa-lg"></i></a></p>
          </div>
         
        
        </div>
      </div>
    </div>
    <div class="text-center text-light" style="background-color: #D90C0C">
      &copy; 2020 - TheClik. Todos os direitos reservados.
    </div>
   
  
  </footer>

</body>
</html>

