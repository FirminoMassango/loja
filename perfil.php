 <?php 
                
             include 'model/Conta.php';
             $conta = new Conta();

             // unset($_SESSION['update']);
             $_SESSION['update'] = false; //Não funciona
             $email = $_SESSION['user'];
             $resultado_conta = $conta->fetch("SELECT * FROM autenticacao WHERE email = '$email'");
    

    ?>



<!DOCTYPE html>
<html>
<head>
  <title>Perfil</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.css" type='text/css'>
  <link rel="stylesheet" href="font-awesome/fonts/FontAwesome.otf">
</head>
<body>


 <div class="container">
   <div class="row justify-content-center">
    
    <h4 class="mt-5 mb-3">Perfil</h4>

    <table class="table table-bordered table-striped table-hover">
      <tbody>
        <tr>
          <td><h6> Foto<h6></td>
          <td><img src="img/produto1.jpg" alt="" width="75px" height="75px"></td>
        </tr>
        <tr>
          <?php while ($row = $resultado_conta->fetch_array()):?>
            <?php $id = $row['id'];?>
          <td><h6>Nome</h6></td>
            
                <td><?php echo $row['nome'];?></td>
          
        </tr>
        <tr>
          <td><h6>E-mail</h6></td>
          <td><?php echo $row['email'];?></td>
        </tr>
        <tr>
          <td><h6>Telefone</h6></td>
          <td><?php echo $row['telefone'];?></td>
        </tr>
        <tr>
          <td><h6>Cidade</h6></td>
          <td><?php echo $row['cidade'];?></td>
        </tr>
        <tr>
          <td><h6>Bairro</h6></td>
          <td><?php echo $row['bairro'];?></td>
        </tr>
        <tr>
          <td><h6>Avenida, rua</h6></td>
          <td><?php echo $row['avenida'];?></td>
        </tr>
       
        <?php endwhile;?> 
          
      </tbody>
    </table>      

  

    <div class="text-center">
     
      <a class="btn btn-success my-2" href="autenticacao/actualizar_perfil.php?id=<?php echo $id?>">Actualizar Informação</a>
    </div>
    

   </div>

 </div>



    <!-- JAVASCRIPT & JQUERY -->
  <script src="js/jquery/jquery.min.js"></script>
  <script src="js/popperjs/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
      
</body>
</html>

<?php  ?>