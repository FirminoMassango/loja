  <?php 
                
             include '../model/Marca.php';
             $marca = new Marca();


             $marca->insert();

    ?>



<!DOCTYPE html>
<html>
<head>
  <title>Criar Marca</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.css" type='text/css'>

  <style>
    body{
      color: #3D3D3D;
    }
   
   
    .campo{
          border: 1px solid #8D8A8A;
          border-radius: 0;

    }
    .formulario{
      border: 1px solid #8D8A8A;
      border-radius: 7px;
    }

    .btn{
      color: #FFF; 
      background-color: #28A745;
	    border: 1px solid #8D8A8A;
      border-radius: 0;
    }

    .btn:hover{
      background-color: #0D952C;
      color: #FFF;
    }
  </style>
</head>
<body>

  <!-- HEADER -->

    <div class="container">
      <div class="row justify-content-center mb-5">
        <div class="col-12 col-md-10 col-lg-8">

            <h4 class="pb-3 my-2 text-center">Criar Marca</h4>
          <form class="formulario py-5 px-5" action="" method="POST">


            <div class="form-row col-12">
           
                <input class="campo form-control my-5 mb-3" type="text" name="campo_marca" id="campoMarca" placeholder="nome da marca">
        
          
                <input class="btn btn-success mb-3" type='submit' name="gravar_marca" value="Confirmar">

            </div>

          </form>       
        </div>      
      </div>
    </div>
 


  <!-- FOOTER -->
   
 
  
    <!-- JAVASCRIPT & JQUERY -->
  <script src="js/jquery/jquery.min.js"></script>
  <script src="js/popperjs/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>

  <script>
      $(function () {
        $('[data-toggle="popover"]').popover()
      })
  </script>
      
</body>
</html>

