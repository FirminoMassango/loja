<!DOCTYPE html>
<html>
<head>
  <title>Painel</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" href="../font-awesome/css/font-awesome.css" type='text/css'>

  <style>
    @font-face { font-family: TeXRegular; src: url('../font/texgyreadventor-regular.otf'); }
    @font-face { font-family: TeXBold; src: url('../font/texgyreadventor-bold.otf'); }

    body{
      color: #3D3D3D;
      font-weight: bold;
    }
   
   
    
  </style>
</head>
<body>

  <!-- HEADER -->

<header>
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-3" id="naveg">
        <div class="container">
          <a class="navbar-brand h1 mb-0" href="#"><img src="../img/logo.svg" style="width: 32px;"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
            <span class="navbar-toggler-icon"></span>
          </button>

           <div class="collapse navbar-collapse" id="navbarSite">

            <ul class="navbar-nav mr-auto">

              <li class="nav-item">
                 <a class="nav-link text-light" href="painel.php">Painel</a>
              </li>
              <li class="nav-item">
                 <a class="nav-link text-light" href="#">Notificações</a>
              </li>
              <li class="nav-item">
                 <a class="nav-link text-light" href="../index.php">Visitar o Site</a>
              </li>

            </ul>


            
          <!-- CONTA -->
              <span class="row">
                 <a href="login.php" class="nav-link text-light">Encerrar Sessão<img class="ml-2" src="../img/sair.svg" width="19px" height="19px"></a>
              </span>

          </div>

        </div>
      </nav>
</header>

<div class="container-fluid">
  <div class="row">
  <div class="col-12 col-sm-4">
    <div class="jumbotron">
     
      <nav id="navbarVertical" class="navbar navbar-light">

      <nav class="nav nav-pills flex-column">

          <div>
            <h6>Adicionar</h6>
            <hr>
          </div>

          <a class="nav-link active" data-toggle="pill" href="#adicionarMarca">Adicionar Marca</a>

          <a class="nav-link" data-toggle="pill" href="#adicionarCategoria">Adicionar Categoria</a>

          <a class="nav-link mb-5" data-toggle="pill" href="#adicionarProduto">Adicionar Produto</a>

          <div>
            <h6>Visualizar</h6>
            <hr>
          </div>

          <a class="nav-link" data-toggle="pill" href="#verCategoria">Ver Categoria</a>

          <a class="nav-link" data-toggle="pill" href="#verProduto">Ver Produto</a>

      </nav>


    </nav>
    
   
    </div>
    
  </div>


    <div class="direita col-sm-8">
        <div class="tab-content" id="nav-pills-content">

          <div class="tab-pane fade show active" id="adicionarMarca" role="tabpanel"> 
            <?php 
                include 'adicionar_marca.php';
             ?>
          </div>

          <div class="tab-pane fade" id="adicionarCategoria" role="tabpanel"> 
            <?php 
                include 'adicionar_categoria.php';
             ?>
          </div>

          <div class="tab-pane fade" id="adicionarProduto" role="tabpanel"> 
             <?php 
                include 'adicionar_produto.php';
             ?>
          </div>
          
    
        </div>
      </div>
  </div>
</div>

  <!-- FOOTER -->
 
 
  
    <!-- JAVASCRIPT & JQUERY -->
  <script src="../js/jquery/jquery.min.js"></script>
  <script src="../js/popperjs/popper.js"></script>
  <script src="../js/bootstrap.min.js"></script>
      
</body>
</html>

