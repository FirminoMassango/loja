 <?php 
             $_SESSION['sucesso']=true;     
             include '../model/Produto.php';
             $produto = new Produto();

             $produto->insert();

             

            $resultado_categoria = $produto->fetch("SELECT * FROM categoria"); 
            $resultado_marca = $produto->fetch("SELECT * FROM marca"); 
      
    ?>

<!DOCTYPE html>
<html>
<head>
  <title>Adicionar Produto</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" href="../font-awesome/css/font-awesome.css" type='text/css'>

  <style>
    body{
      color: #3D3D3D;
    }
   
   
    .campo{
          border: 1px solid #8D8A8A;
          border-radius: 0;

    }
    .formulario{
      border: 1px solid #8D8A8A;
      border-radius: 7px;
    }

    .btn{
      color: #FFF; 
      background-color: #28A745;
      border: 1px solid #8D8A8A;
      border-radius: 0;
    }

    .btn:hover{
      background-color: #0D952C;
      color: #FFF;
    }
  </style>
</head>
<body>

  <!-- HEADER -->



      <?php
        if ($_SESSION['sucesso'] = true):
      ?>

      <div class="py-3 bg-success text-light text-center mb-5">
          <h6 class="text-light">Produto adicionado com sucesso</h6>
      </div>

       <?php
        endif;
      ?>
   
    <div class="container">
      <div class="row justify-content-center mb-5">
        <div class="col-12 col-md-10 col-lg-8">

          
            <h4 class="pb-3 my-2 text-center">Adicionar Produto</h4>




          <form class="formulario py-5 px-5" action="" method="POST" enctype="multipart/form-data">

            <div class="form-row">
            
              <div class="col-sm-12">
                <input class="campo form-control mb-3" type="text" name="campo_nome" id="campoNome" placeholder="nome do produto">
              </div>    

              <div class="col-sm-6">
                <select class="campo form-control mb-3" name="campo_marca" id="marca">
                  <option selected>Marca...</option>
                 <?php while ($row = $resultado_marca->fetch_array()):?>
                    <option value=<?php echo $row['nome_marca'];?>><?php echo $row['nome_marca'];?></option>
                  <?php endwhile;?>  <!-- FIM PHP -->
                </select>
              </div>

              <div class="col-sm-6">
                <select class="campo form-control mb-3" name="campo_categoria" id="categoria">
                  <option selected>Categoria...</option>
                       <!-- PHP -->
                  <?php while ($row = $resultado_categoria->fetch_array()):?>
                    <option value=<?php echo $row['nome_categoria'];?>><?php echo $row['nome_categoria'];?></option>
                  <?php endwhile;?>  <!-- FIM PHP -->
                </select>
              </div>

              <div class="col-sm-6">
                <input class="campo form-control mb-3" type="number" name="campo_quantidade" id="campoQtd" placeholder="quantidade" min="1">
              </div>

              <div class="col-sm-6">
                <input class="campo form-control mb-3" type="text" name="campo_preco" id="campoPreco" placeholder="preço" min="0">
              </div>

              <div class="col-sm-12">
                <div class="form-group">
                  <label for="imagem">Imagem:</label>
                  <input class="campo form-control col-md-12" type="file" name="uploaded_file" id="campoFicheiro" placeholder="imagem" style="height: auto;">
                </div>
              </div>

              <div class="col-sm-12">
                <textarea class="campo form-control mb-3" name="campo_descricao" id="campoDescricao" rows="5"></textarea>
              </div>

              <div class="col-sm-12">
                <input class="btn btn-success" type="submit" name='gravar_produto' value="Confirmar">
              </div>

            </div>

          </form>       
        </div>      
      </div>
    </div>



  <!-- FOOTER -->
   
 
  
    <!-- JAVASCRIPT & JQUERY -->
  <script src="../js/jquery/jquery.min.js"></script>
  <script src="../js/popperjs/popper.js"></script>
  <script src="../js/bootstrap.min.js"></script>

      
</body>
</html>

