<!DOCTYPE html>
<html>
<head>
  <title>Loja</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/header.css">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.css" type='text/css'>
</head>
<body>


  <!-- NAVBAR -->
    <header>
      <nav class="navbar navbar-expand-lg navbar-dark" id="naveg">
        <div class="container">
          <a class="navbar-brand h1 mb-0" href="#"><img src="img/logo.svg" style="width: 32px;"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
            <span class="navbar-toggler-icon"></span>
          </button>

           <div class="collapse navbar-collapse" id="navbarSite">

            <ul class="navbar-nav mr-auto">

              <li class="nav-item">
                <a class="nav-link text-light ml-4" href="index.php">Início</a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-light ml-4" href="contacto.php">Contacto</a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-light ml-4" href="#">Sobre Nós</a>
              </li>

            </ul>

            <!-- PHP -->

            <?php
              session_start();
            // session_destroy();
            ?>

            <?php
              if (isset($_SESSION['user'])):
            ?>
              
              <a class="nav-link text-dark mr-5" href="perfil.php"><i class="fa fa-user text-light mr-2"></i><?php echo $_SESSION['user'];?></a>

            <?php
              endif;
            ?>

            <!-- FIM PHP -->
            
          <!-- CONTA -->
              <span class="row">
                 <a href="autenticacao/login.php" class="nav-link text-light">Iniciar Sessão</a> 
                 <span class="nav-link text-light">|</span>
                 <a href="autenticacao/nova_conta.php" class="nav-link text-light">Criar Conta</a>
              </span>
             
            
           
          
          </div>
          
          
          

        </div>
      </nav>
    </header>

  

  <!-- RODAPÉ -->
 

  
    <!-- JAVASCRIPT & JQUERY -->
  <script src="js/jquery/jquery.min.js"></script>
  <script src="js/popperjs/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
      
</body>
</html>

