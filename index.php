<?php
  include 'header.php';
  include 'model/Produto.php';
     $produto = new Produto();
     $limite = 4; //Limite para a listagem dos produtos na aba novo
     $limite_todos = 20; //Limite para a listagem dos produtos na aba outros
     $resultado_novo = $produto->fetch("SELECT * FROM produto ORDER BY id DESC LIMIT $limite");
     $resultado_outros = $produto->fetch("SELECT * FROM produto LIMIT $limite_todos");
     $resultado_categoria = $produto->fetch("SELECT * FROM categoria"); //Nesse caso não foi necessário importar a classe Categoria pois a base de dados é a mesma que da classe Produto


    //total de produtos existentes
    $total = $produto->fetch("SELECT COUNT(id) AS id FROM produto");
    $total = $total->fetch_all(MYSQLI_ASSOC);
    $total = $total[0]['id'];

  

    // if (){

    // }


?>
<!DOCTYPE html>
<html>
<head>
  <title>Loja</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.css" type='text/css'>
  <link rel="stylesheet" href="font-awesome/fonts/FontAwesome.otf">

</head>
<body>

  <!-- CAROUSEL -->
  <div id="carouselSite" class="carousel slide" data-ride="carousel">

    <ol class="carousel-indicators">
      <li data-target="#carouselSite" data-slide-to="0" class="active"></li>
      <li data-target="#carouselSite" data-slide-to="1"></li>
      <li data-target="#carouselSite" data-slide-to="2"></li>
    </ol>

    <div class="carousel-inner">

      <div class="carousel-item">
        <img src="img/slide1.jpg" class="img-fluid d-block">

        <div class="carousel-caption d-none d-md-block text-dark">
          <h3>Lorem ipsum dolor</h3>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
        </div>

      </div>

      <div class="carousel-item active">
        <img src="img/slide2.jpg" class="img-fluid d-block">

        <div class="carousel-caption d-none d-md-block">
          <h3>Nemo enim ipsam</h3>
          <p>Ut enim ad minima veniam, quis nostrum exercitationem ullam</p>
        </div>

      </div>

      <div class="carousel-item">
        <img src="img/slide3.jpg" class="img-fluid d-block">

        <div class="carousel-caption d-none d-md-block">
          <h3>At vero eos et</h3>
          <p>Nam libero tempore, cum soluta nobis est eligendi</p>
        </div>

      </div>

    </div>

    <a class="carousel-control-prev" href="#carouselSite" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
      <span class="sr-only">Anterior</span>
    </a>


    <a class="carousel-control-next" href="#carouselSite" role="button" data-slide="next">
      <span class="carousel-control-next-icon"></span>
      <span class="sr-only">Próximo</span>
    </a>

  </div>

  <!-- TÍTULO DA PÁGINA -->
  <div class="container">

    <div class="row">
       
      <div class="col-12 text-center my-5">

          <h3 class="display-4">As melhores novidades estão aqui.</h3>
          <h4>Melhores preços do mercado.</h6>

      </div>

    </div>
  </div>
  
  <div class="container">
    <div class="row justify-content-center my-5">

      <!-- PESQUISAR -->
      <form class="form-inline" action="" method="POST">
        <input class="pesquisar mr-2 col-9 col-lg-9" name="pesquisar" type="search" title="Pesquise o produto que deseja" placeholder="Pesquisar produto...">
        <button class="btn btn-danger" name="btn_pesquisar" type="Submit"><img src="img/pesquisar.svg" style="width: 20px;"></button>
      </form>
    </div>  
  </div>
    

 <div class="container-fluid">
   <div class="row">
    
     <!-- LADO ESQUERDO -->
    <div class="lateral-esquerdo col-12 col-sm-4 mb-3" id="pp">
      <h4 class="mt-5">Categorias</h4>
        <ul class="list-group">
          <li class="list-group-item d-flex justify-content-between align-items-center active">
            Todos Produtos
            <span class="badge badge-secondary badge-pill"><?php echo $total?></span>
          </li>

                <!-- PHP -->
           <?php while ($row = $resultado_categoria->fetch_array()):?>
               <li class="list-group-item d-flex justify-content-between align-items-center">
                    <?php echo $row['nome_categoria'];?>
                    <span class="badge badge-secondary badge-pill">
                      <?php 
                            $categoria = $row['nome_categoria'];
                            $categ = $produto->fetch("SELECT COUNT(id) AS id FROM produto WHERE categoria = '$categoria'");
                            $categ = $categ->fetch_all(MYSQLI_ASSOC);
                            echo $categ = $categ[0]['id'];
                      ?>
                     </span>
               </li>

            <?php endwhile;?>  <!-- FIM PHP -->


     </ul>

    </div>
  
    <!-- LADO DIREITO -->
    <div class="lateral-direito col-12 col-sm-8 mt-5">
       

    	<?php
  
		  if (isset($_POST['btn_pesquisar'])){

		    $chave = $_POST['pesquisar'];

		    // include 'model/Produto.php';
		     // $produto = new Produto();
		    


		     $pesquisa = $produto->fetch("SELECT * FROM produto WHERE nome LIKE '%$chave%'");

		     $resultado = $produto->fetch("SELECT COUNT(id) AS id FROM produto WHERE nome LIKE '%$chave%'");
		     $resultado = $resultado->fetch_all(MYSQLI_ASSOC);
		     $resultado = $resultado[0]['id'];


		         if ($resultado == 0){
		          echo "Produto não encontrado, talvez tenha usado palavra-chave incompatível";
		         }else{
		          include 'view_pesquisa.php';
		         }
		               
		  } else{
		  	
		  	include 'listar_produtos.php';

		  }
?>


      


   </div>



 </div>
  
 <!-- MODAL -->
 <div class="modal fade" id="siteModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Excepteur sint occaecat</h5>
        <button type="button" class="close" data-dismiss="modal">
          <span>&times;</span>
        </button>
      </div>

      <div class="modal-body">
       <div class="alert alert-danger" role="alert">
          <i class="fa fa-info-circle"></i>
          Produto esgotado
       </div>
       
       <div class="container">
        <div class="row">
       
          <div class="col-12 col-md-6 mb-3">
            <img src="img/produto3.jpg" alt="iphone11" class="img-thumbnail">
          </div>

          <div class="col-12 col-md-6">
            <p><strong>Preço: </strong>€ 1.195,00 EUR</p>
            <hr>
            <p><strong>Descrição:</strong></p>
            <pre>
            Sistema Operativo: iOS 13
            Disponibilidade: 2019/3
            RAM: 4GB
            Memória: 512GB
            Peso:  226 gramas
            Resistência a água: Sim</pre>
            <hr>
             <form action="">
               <div class="form-group">
                  <label for="">Cor:</label>
                  <select class="form-control" name="" id="">
                    <option value="preto">Preto</option>
                    <option value="branco">Branco</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Quantidade:</label>
                  <select class="form-control" name="" id="">
                    <!-- POPULANDO O SELECT -->
                    <?php 
                      for ($i = 1; $i<10; $i++):
                    ?>

                    <option value="preto"><?php echo $i;?></option>

                    <?php 
                       endfor;
                    ?>
                    <!-- FIM DO FOR -->
                  </select>
                </div>

             
            </form> 

           
          </div>
          
        </div>  
       </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Temporibus</button>
        <button type="button" class="btn btn-primary"><img src="img/carrinha.svg" style="width: 28px;">Comprar</button>
      </div>
    </div>
  </div>
</div>

  <!-- RODAPÉ -->
  <?php
    include 'footer.php';
  ?> 

    <!-- JAVASCRIPT & JQUERY -->
  <script src="js/jquery/jquery.min.js"></script>
  <script src="js/popperjs/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
      
</body>
</html>

