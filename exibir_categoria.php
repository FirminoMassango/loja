<!DOCTYPE html>
<html>
<head>
  <title>Loja</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.css" type='text/css'>
  <link rel="stylesheet" href="font-awesome/fonts/FontAwesome.otf">
</head>
<body>


 <div class="container-fluid">
   <div class="row">
    
     <!-- LADO ESQUERDO -->
    <div class="lateral-esquerdo col-12 col-sm-4 mb-3" id="pp">
      <h4 class="mt-5">Categorias</h4>
        <ul class="list-group">
          <li class="list-group-item d-flex justify-content-between align-items-center active">
            Todos
            <span class="badge badge-secondary badge-pill">100</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Hombre
            <span class="badge badge-secondary badge-pill">4</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Mujer
            <span class="badge badge-secondary badge-pill">12</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Hipster
            <span class="badge badge-secondary badge-pill">24</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Metal
            <span class="badge badge-secondary badge-pill">6</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Universidad
            <span class="badge badge-secondary badge-pill">5</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Deportiva
            <span class="badge badge-secondary badge-pill">8</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Rayas
            <span class="badge badge-secondary badge-pill">11</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Apretada
            <span class="badge badge-secondary badge-pill">19</span>
          </li>
        </ul>

    </div>
  
    <!-- LADO DIREITO -->
    <div class="lateral-direito col-12 col-sm-8 mt-5">
        <div class="row col-12 col-sm-4 ml-2">
          <div class="mb-3">
            <h6>TELEMÓVEIS</h6>
            <div class="mb-3" id="underline-other"></div>
          </div>
        </div>
        
      <div class="row justify-content-sm-center">
        <div class="produto mb-4 ml-2 mr-2">
          <img class="imgProduto mb-2" src="img/produto2.png" alt="produto" width="200px" height="200px">
            <p class="modelo mb-1">Samsung S10</p>
            <p class="preco">70.000 MT</p>
            <!-- <a href="#" class="btn btn-success" data-toggle="modal" data-target="#siteModal">Detalhes</a> -->
            <button id="detalhes" data-toggle="modal" data-target="#siteModal">DETALHES</button>
        </div>
        <div class="produto mb-5 ml-2 mr-2">
          <img class="imgProduto mb-2" src="img/produto1.jpg" alt="produto" width="200px" height="200px">
            <p class="modelo mb-1">Xiaomi Mi 8</p>
            <p class="preco">65.000 MT</p>
            <button id="detalhes">DETALHES</button>
        </div>
        <div class="produto mb-5 ml-2 mr-2">
          <img class="imgProduto mb-2" src="img/produto3.jpg" alt="produto" width="200px" height="200px">
            <p class="modelo mb-1">iPhone 11 Pro</p>
            <p class="preco">100.000 MT</p>
            <button id="detalhes">DETALHES</button>
        </div>
        <div class="produto mb-5 ml-2 mr-2">
          <img class="imgProduto mb-2" src="img/produto2.png" alt="produto" width="200px" height="200px">
            <p class="modelo mb-1">Samsung S10</p>
            <p class="preco">70.000 MT</p>
            <button id="detalhes">DETALHES</button>
        </div>
        <div class="produto mb-5 ml-2 mr-2">
          <img class="imgProduto mb-2" src="img/produto2.png" alt="produto" width="200px" height="200px">
            <p class="modelo mb-1">Samsung S10</p>
            <p class="preco">70.000 MT</p>
            <!-- <a href="#" class="btn btn-success" data-toggle="modal" data-target="#siteModal">Detalhes</a> -->
            <button id="detalhes" data-toggle="modal" data-target="#siteModal">DETALHES</button>
        </div>
        <div class="produto mb-5 ml-2 mr-2">
          <img class="imgProduto mb-2" src="img/produto1.jpg" alt="produto" width="200px" height="200px">
            <p class="modelo mb-1">Xiaomi Mi 8</p>
            <p class="preco">65.000 MT</p>
            <button id="detalhes">DETALHES</button>
        </div>
        <div class="produto mb-5 ml-2 mr-2">
          <img class="imgProduto mb-2" src="img/produto3.jpg" alt="produto" width="200px" height="200px">
            <p class="modelo mb-1">iPhone 11 Pro</p>
            <p class="preco">100.000 MT</p>
            <button id="detalhes">DETALHES</button>
        </div>
        <div class="produto mb-5 ml-2 mr-2">
          <img class="imgProduto mb-2" src="img/produto2.png" alt="produto" width="200px" height="200px">
            <p class="modelo mb-1">Samsung S10</p>
            <p class="preco">70.000 MT</p>
            <button id="detalhes">DETALHES</button>
        </div>
       
        
      </div>

      <!-- PAGINAÇÃO -->
      <div class="row justify-content-center mb-5">
        <div class="paginacaoManual">
          <div class="row">
            <a href="#" title="Anterior"><i class="fa fa-arrow-left mr-1 text-secondary"></i></a>
            <span class="ml-3 mr-3">1 / 5</span>
            <a href="#" title="Próximo"><i class="fa fa-arrow-right ml-1 text-secondary"></i></a>
          </div>
        </div>
      </div>

    </div>

   </div>

 </div>
  
 <!-- MODAL -->
 <div class="modal fade" id="siteModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Excepteur sint occaecat</h5>
        <button type="button" class="close" data-dismiss="modal">
          <span>&times;</span>
        </button>
      </div>

      <div class="modal-body">
       <div class="alert alert-danger" role="alert">
          <i class="fa fa-info-circle"></i>
          Produto esgotado
       </div>
       
       <div class="container">
        <div class="row">
       
          <div class="col-12 col-md-6 mb-3">
            <img src="img/produto3.jpg" alt="iphone11" class="img-thumbnail">
          </div>

          <div class="col-12 col-md-6">
            <p><strong>Preço: </strong>€ 1.195,00 EUR</p>
            <hr>
            <p><strong>Descrição:</strong></p>
            <pre>
            Sistema Operativo: iOS 13
            Disponibilidade: 2019/3
            RAM: 4GB
            Memória: 512GB
            Peso:  226 gramas
            Resistência a água: Sim</pre>
            <hr>
             <form action="">
               <div class="form-group">
                  <label for="">Cor:</label>
                  <select class="form-control" name="" id="">
                    <option value="preto">Preto</option>
                    <option value="branco">Branco</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Quantidade:</label>
                  <input class="form-control" type="number" min="1" value="1">
                </div>
             
            </form> 

           
          </div>
          
        </div>  
       </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Temporibus</button>
        <button type="button" class="btn btn-primary"><img src="img/carrinha.svg" style="width: 28px;">Comprar</button>
      </div>
    </div>
  </div>
</div>


    <!-- JAVASCRIPT & JQUERY -->
  <script src="js/jquery/jquery.min.js"></script>
  <script src="js/popperjs/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
      
</body>
</html>

