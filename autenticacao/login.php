
<?php
             session_start(); 
             include '../model/Login.php';
             $login = new Login();

             $login->login();

?>

<!DOCTYPE html>
<html>
<head>
  <title>Autenticacao</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" href="../font-awesome/css/font-awesome.css" type='text/css'>

  <style>
    @font-face { font-family: TeXRegular; src: url('../font/texgyreadventor-regular.otf'); }
    @font-face { font-family: TeXBold; src: url('../font/texgyreadventor-bold.otf'); }


    body{
      color: #3D3D3D;
      font-family: TeXRegular;
    }
   
   
    .campo{
          border: 1px solid #8D8A8A;
          border-radius: 0;
    }
    .formulario{
      border: 1px solid #8D8A8A;
      border-radius: 7px;
    }

    .btn{
      color: #FFF; 
      background-color: #28A745;
	    border: 1px solid #8D8A8A;
      border-radius: 0;
    }

    .btn:hover{
      background-color: #0D952C;
      color: #FFF;
    }
  </style>
</head>
<body>

  <!-- HEADER -->
  <?php
    if (empty($_SESSION['user'])):
  ?>

    <div class="py-3 bg-danger text-light text-center">
      <h6 class="text-light">E-mail ou senha incorrectos</h6>
    </div>

  <?php
    else:
  ?>

  <div class="py-3 bg-success text-light text-center">
      <h6 class="text-light">Autenticado com sucesso</h6>
  </div>

   <?php
    endif;
  ?>
 
  <div class="container">
  <div class="row justify-content-center col-12 mt-5">
    <form class="formulario mt-5 py-5 px-5" action="" method="POST">
      <h4 class="pb-3 text-center">Autenticação</h4>
      <div class="form-group">
        <input class="campo form-control" type="text" name="email" id="campoEmail" placeholder="digite o seu email">
      </div>
      <div class="form-group">
        <input class="campo form-control" type="password" name="senha" id="campoSenha" placeholder="digite sua senha">
      </div>
      
      <input class="btn form-control" type="submit" name="login" value="Confirmar">
    
    </form>
  </div>
  </div>


  <!-- FOOTER -->
 
 
  
    <!-- JAVASCRIPT & JQUERY -->
  <script src="../js/jquery/jquery.min.js"></script>
  <script src="../js/popperjs/popper.js"></script>
  <script src="../js/bootstrap.min.js"></script>
      
</body>
</html>

