 <?php 
                
             include '../model/Conta.php';
             $editar_conta = new Conta();

             $editar_conta->insert();

              $id = $_REQUEST['id'];
              $resultado = $editar_conta->fetch("SELECT * FROM autenticacao WHERE id = $id");
              
                $_SESSION['update'] = false;
              
              // if (isset($_POST['actualizar'])) {
              $actualizar = $editar_conta->update($id);

 
               while ($row = $resultado->fetch_array()){
                $nome = $row['nome'];
                $email = $row['email'];
                $telefone = $row['telefone'];
                $cidade = $row['cidade'];
                $bairro = $row['bairro'];
                $avenida = $row['avenida'];
                $senha = $row['senha'];
               }
                      // session_start();

    ?>



<!DOCTYPE html>
<html>
<head>
  <title>Actualizar Perfil</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" href="../font-awesome/css/font-awesome.css" type='text/css'>

  <style>
    body{
      color: #3D3D3D;
    }
   
   
    .campo{
          border: 1px solid #8D8A8A;
          border-radius: 0;

    }
    .formulario{
      border: 1px solid #8D8A8A;
      border-radius: 7px;
    }

    .btn{
      color: #FFF; 
      background-color: #28A745;
	    border: 1px solid #8D8A8A;
      border-radius: 0;
    }

    .btn:hover{
      background-color: #0D952C;
      color: #FFF;
    }
  </style>
</head>
<body>

  <!-- HEADER -->
 

      <?php
        if ($_SESSION['update'] = true):
      ?>

      <div class="py-3 bg-success text-light text-center">
          <h6 class="text-light">Perfil actualizado com sucesso | <?php echo $_SESSION['update'] ?></h6>
      </div>

       <?php
        endif;
      ?>
   






    <div class="container">
      <div class="row justify-content-center my-5">
        <div class="col-12 col-md-10 col-lg-8">
          <form class="formulario mt-5 py-5 px-5" action="" method="POST">

            <h4 class="pb-3 text-center">Actualizar Perfil</h4>

            <div class="form-row">

              <div class="col-sm-6">
                <input class="campo form-control mb-3" type="text" name="nome" id="campoNome" value="<?php echo $nome?>" placeholder="nome">
              </div>
              
              <div class="col-sm-6">
                <input class="campo form-control mb-3" type="text" name="email" id="campoEmail" value="<?php echo $email?>" placeholder="email">
              </div>

              <div class="col-sm-6">
                <input class="campo form-control mb-3" type="text" name="telefone" id="campoTelefone" value="<?php echo $telefone?>" placeholder="número de telefone">
              </div>

              <div class="col-sm-6">
                <select class="campo form-control mb-3" name="cidade" id="cidade">

                  <?php if ($cidade == 'Maputo'):?>
                  <option value="Maputo" <?php echo 'selected'?> >Maputo</option>
                  <option value="Matola" >Matola</option>
                  
                  <?php elseif($cidade == 'Matola'):?>
                     <option value="Maputo" >Maputo</option>
                  <option value="Matola" <?php echo 'selected'?> >Matola</option>
                  <?php endif;?>
                </select>
              </div>

              <div class="col-sm-6">
                <input class="campo form-control mb-3" type="text" name="bairro" id="campoBairro"  value="<?php echo $bairro?>" placeholder="bairro">
              </div>

              <div class="col-sm-6">
                <input class="campo form-control mb-3" type="text" name="avenida" id="campoAvenida"  value="<?php echo $avenida?>" placeholder="avenida, rua">
              </div>

              <div class="col-sm-6">
                <input class="campo form-control mb-3" type="password" name="senha" id="campoSenha" value="<?php echo $senha?>" placeholder="senha" title="A senha parece maior por questões de segurança">
              </div>

              <div class="col-sm-6">
                <input class="campo form-control mb-3" type="password" name="nova_senha" id="campoSenha" value="<?php echo $senha?>" placeholder="confirme a senha" title="A senha parece maior por questões de segurança">
              </div>

            </div>
            <div class="form-row">
              <div class="col-12">
                <input class="btn" type="submit" value="Actualizar" name="actualizar">
              </div>
            </div>

          </form>       
        </div>      
      </div>
    </div>
 


  <!-- FOOTER -->
 
 
  
    <!-- JAVASCRIPT & JQUERY -->
  <script src="js/jquery/jquery.min.js"></script>
  <script src="js/popperjs/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
      
</body>
</html>

