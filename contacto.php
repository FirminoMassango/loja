    <!-- HEADER -->
   <?php
    include 'header.php';
  ?> 

<!DOCTYPE html>
<html>
<head>
  <title>Contacto</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.css" type='text/css'>

  <style>
    body{
      color: #3D3D3D;
    }
    .campo{
          border: 1px solid #8D8A8A;
          border-radius: 0;
    }
    .btn{
      /* background-color: #FFF; */
      /* color: #000; */
	    border: 1px solid #8D8A8A;
      border-radius: 0;
    }
  </style>
</head>
<body>

 



  <div class="container">
    <div class="col-sm-12 ml-5 my-5">
    <h4>THE CLIK</h4>
    <p>Em caso de dúvidas ou sugestões sinta-se a vontade para entrar em contacto connosco ...
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
      ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
      laboris nisi ut aliquip ex ea commodo consequat. 
    </p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
      ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
      laboris nisi ut aliquip ex ea commodo consequat. </p>
  
    <h4 class="my-5">Contacte-nos</h4>
    </div>
    
  
  </div>
 
  <div class="container">
  <div class="row justify-content-center col-12">
    <form action="">
      <div class="form-group">
        <label for="">Nome</label>
        <input class="campo form-control" type="text" name="nome" id="campoNome" placeholder="digite o seu nome">
      </div>
      <div class="form-group">
        <label for="">E-mail</label>
        <input class=" campo form-control" type="text" name="nome" id="campoEmail" placeholder="digite o seu e-mail">
      </div>
      <div class="form-group">
        <label for="">Telefone</label>
        <input class="campo form-control" type="text" name="nome" id="campoTelefone" placeholder="digite o seu número de telefone">
      </div>
      <div class="form-group">
        <label for="">Mensagem</label>
        <textarea class="campo form-control" name="campoMensagem" id="" cols="80" placeholder="mensagem"></textarea>
      </div>

      <input class="btn btn-success mb-5" type="submit" value="Enviar">
    
    </form>
  </div>
  </div>
 


 
    <!-- JAVASCRIPT & JQUERY -->
  <script src="js/jquery/jquery.min.js"></script>
  <script src="js/popperjs/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
      
</body>

  
  <!-- FOOTER -->
   <?php 
    include 'footer.php';
   ?>
</html>

