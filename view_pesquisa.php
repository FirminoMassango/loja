<!DOCTYPE html>
<html>
 <title>Resultado Pesquisa</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.css" type='text/css'>
  <link rel="stylesheet" href="font-awesome/fonts/FontAwesome.otf">


<body>
  
    <!-- LADO DIREITO -->
     
     
     <div class="container">
       <div class="row justify-content-sm-center my-5" id="prod">

                  <!-- PHP -->
          
            <!-- PHP -->
    <?php while ($row = $pesquisa->fetch_array()):?>
        <div class="produto mb-4 ml-2 mr-2 text-center">
          <img class="imgProduto mb-2" src=<?php echo "admin/produtos/".$row['imagem'];?> alt="produto" width="200px" height="200px">
            <p class="modelo mb-1"><?php echo $row['nome'];?></p>
            <p class="preco"><?php echo number_format($row['preco'],2,'.','.');?> MT</p>
            <a id="detalhes" data-toggle="modal" data-target="#siteModal">DETALHES</a>
        </div>

             <!-- FIM PHP --> <?php endwhile;?>   <!-- FIM PHP -->

             <?php unset($_POST['btn_pesquisar']);?>


 
  

    </div>

     </div>
      




  




    <!-- JAVASCRIPT & JQUERY -->
  <script src="js/jquery/jquery.min.js"></script>
  <script src="js/popperjs/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
      
</body>
</html>

